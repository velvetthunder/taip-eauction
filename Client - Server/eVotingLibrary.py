import math

# GLOBAL VARIABLES
m = 0x1d7777c38863aec21ba2d91ee0faf51
e = 0x5abb
d = 0x1146bd07f0b74c086df00b37c602a0b

base = 10
expYes = 2
expNo = 3

def RSA_Encrypt_Messages(messageType):
    if messageType == "yes":
        return pow(pow(base,expYes), e, m)
    if messageType == "no":
        return pow(pow(base,expNo), e, m)

def RSA_Decrypt_Messages(encryptedMessage):
    return pow(encryptedMessage, d, m)

def Get_Voting_Results(base,nr,total):
    sum= math.log(total,base)
    x = (sum-expNo*nr)/(expYes-expNo)
    if(x>(nr-x)):
        return "Yes"
    return "No"