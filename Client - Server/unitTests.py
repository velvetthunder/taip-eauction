# import unittest
# from Pyfhel import Pyfhel
# from Pyfhel import PyCtxt

# pyfhel = Pyfhel()
# pyfhel.contextGen(p=65537)
# pyfhel.keyGen()


# def encryptInt(value):
#     ctxt = PyCtxt()
#     pyfhel.encryptInt(value, ctxt)
#     return ctxt

# def decryptInt(value):
#     return pyfhel.decryptInt(value)

# def sumInt(value1,value2):
#     return pyfhel.add(value1,value2)

# def difInt(value1,value2):
#     return pyfhel.sub(value1,value2)

# def greater(value1,value2):
#     pyfhel.sub(value1,value2)
#     if(decryptInt(value1)>0):
#         pyfhel.add(value1,value2)
#         return value1
#     elif(decryptInt(value1)<0):
#         return value2
#     else:
#         return value1

# def max(values):
#     max = values[0]
#     for i in (1,len(values)-1):
#         if(greater(max,values[i])==values[i]):
#             max=values[i]

#     return max

# class MyTest(unittest.TestCase):
#     def testDecryption(self):
#         value = 127
#         cryptoText = encryptInt(value)
#         self.assertEqual(decryptInt(cryptoText), 127)

#     def testHomomorficSum(self):
#         value1=127
#         value2=103
#         c1=encryptInt(value1)
#         c2=encryptInt(value2)
#         self.assertEqual(decryptInt(sumInt(c1,c2)),230)
    
#     def testHomomorficDif(self):
#         value1=127
#         value2=103
#         c1=encryptInt(value1)
#         c2=encryptInt(value2)
#         self.assertEqual(decryptInt(difInt(c1,c2)),24)

#     def testHomomorficGreater_Case_Value1_Greater(self):
#         value1=127
#         value2=103
#         c1=encryptInt(value1)
#         c2=encryptInt(value2)
#         self.assertEqual(decryptInt(greater(c1,c2)),127)

#     def testHomomorficGreater_Case_Value2_Greater(self):
#         value1=100
#         value2=103
#         c1=encryptInt(value1)
#         c2=encryptInt(value2)
#         self.assertEqual(decryptInt(greater(c1,c2)),103)

#     def testHomomorficGreater_Case_Equal(self):
#         value1=103
#         value2=103
#         c1=encryptInt(value1)
#         c2=encryptInt(value2)
#         self.assertEqual(decryptInt(greater(c1,c2)),0)

#     def testMax(self):
#         values=[encryptInt(1),encryptInt(3),encryptInt(2)]
#         self.assertEqual(decryptInt(max(values)),3)

# if __name__ == '__main__':
#     unittest.main()
