import logging

def initializeLogger():
    logger = logging.getLogger('eauction')
    logger.setLevel(logging.DEBUG)

    fh = logging.FileHandler('serverlog.txt')
    fh.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fh.setFormatter(formatter)
    logger.addHandler(fh)
    return logger
