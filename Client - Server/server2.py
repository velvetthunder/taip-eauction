from socket import socket
from socket import AF_INET
from socket import SOCK_STREAM
from threading import Thread
import aspects
import time


clients = []
clientNumbers = []
numberOfBids = 0

items_array = []

logger = aspects.initializeLogger()

def multiply(list):
    p = 1
    for i in list:
        p*=i

    return p


def clientHandler(c, addr):
    global clients
    global clientNumbers
    global numberOfBids
    global items_array
    current_item = 0
    print(addr, "is Connected")

    logger.info("Client with address" + str(addr) + "is connected")

    try:
        c.sendto(participantsNumber.encode(), addr)
        while True:
            print("Sending item number "+ str(current_item))
            c.sendto(items_array[current_item].encode(), addr)

            data = c.recv(1024)
            clientNumbers.append(int(data))

            logger.info("Client with address " + str(addr) + " voted " + str(data.decode()))

            # print(clientNumbers)
            numberOfBids += 1
            while True:
                if not data:
                    break
                if numberOfBids == int(participantsNumber):
                    prod = multiply(clientNumbers)
                    c.sendto(str(prod).encode(), addr)

                    current_item+=1
                    break

            time.sleep(3)
            numberOfBids=0
            clientNumbers.clear()

            # logger.info("Voting has ended with the majority vote: " +  str(max(clientNumbers).decode()))

            if current_item==len(items_array):
                c.sendto("EndVote".encode(), addr)
        # logger.info("Voting has ended" )


    except:
        print("Error. Data not sent to all clients.")
    finally:
        c.close()

HOST = '' #localhost
PORT = 10000

participantsNumber = input('Enter number of participants: ')

s = socket(AF_INET, SOCK_STREAM)
s.bind((HOST, PORT))
s.listen(int(participantsNumber))

print("Server is running on "+ str(PORT))

print("Loading items...")
logger.info("Loading items...")
with open('items.txt') as my_file:
    items_array = my_file.readlines()
print("Items loaded successfuly... "+str(len(items_array)))
logger.info("Items loaded successfuly... "+str(len(items_array)))

trds = []

for i in range(int(participantsNumber)):
    c, addr = s.accept()
    clients.append(addr)
    t = Thread(target=clientHandler, args = (c, addr))
    trds.append(t)
    t.start()

for t in trds:
    t.join()

s.close()
