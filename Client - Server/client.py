import socket
import sys
import math
import eVotingLibrary

m = 0x1d7777c38863aec21ba2d91ee0faf51
e = 0x5abb
d = 0x1146bd07f0b74c086df00b37c602a0b

base = 10
expYes = 2
expNo = 3

# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Connect the socket to the port where the server is listening
server_address = ('localhost', 10000)
print('connecting to %s port %s' % server_address)
sock.connect(server_address)

try:
    data = sock.recv(1024)
    # print( 'received "%s"' % data.decode())
    nr = int(data.decode())

    data = sock.recv(1024)
    while data.decode() !="EndVote":
        print("----------")
        print("Voting object: "+ data.decode() +"\n")
        # Send data
        # message = 'This is the message.  It will be repeated.'
        isOk=True
        while(isOk):
            vote = input('Enter \'yes\' / \'no\': ')
            message = ''
            if(vote == "yes"):
                message = eVotingLibrary.RSA_Encrypt_Messages("yes")
                isOk=False
            elif(vote == "no"):
                message = eVotingLibrary.RSA_Encrypt_Messages("no")
                isOk=False
            else:
                print("Invalid vote. Please re-enter valid option!")

        print( 'sending encoded "%s"' % message)
        sock.sendall(str(message).encode())

        # Look for the response
        # amount_received = 0
        # amount_expected = len(message)

        # while True:
        data = sock.recv(1024)

        print('received "%s"' % data.decode())
        encryptedResult = int(data.decode())
        decProd = eVotingLibrary.RSA_Decrypt_Messages(encryptedResult)
        # print("Prod: "+ str(decProd))

        resultOfRound = eVotingLibrary.Get_Voting_Results(base,nr,decProd)
        # print(nr_yes, "voted \'yes\'")

        print("The majority voted " + resultOfRound +".\n Next round starts in 3 seconds!")
        data = sock.recv(1024)


finally:
    print('closing socket')
    sock.close()