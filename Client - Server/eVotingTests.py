import unittest
import eVotingLibrary

class eVotingTest(unittest.TestCase):
    def test_RSA_Encrypt_Messages_With_Yes_Vote(self):
        self.assertEqual(eVotingLibrary.RSA_Encrypt_Messages("yes"), 1682815935317023032846185376704303182)

    def test_RSA_Encrypt_Messages_With_No_Vote(self):
        self.assertEqual(eVotingLibrary.RSA_Encrypt_Messages("no"), 124025312978133866405474645655006820)
    
    def test_RSA_Decrypt_Messages(self):
        self.assertEqual(eVotingLibrary.RSA_Decrypt_Messages(1682815935317023032846185376704303182), 100)
    
    def test_Get_Voting_Results_With_Yes_Majority(self):
        self.assertEqual(eVotingLibrary.Get_Voting_Results(eVotingLibrary.base,3,10000000), "Yes")
        
if __name__ == '__main__':
    unittest.main()